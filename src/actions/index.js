export const selectChapter = (chapterId) => {
	return {
		type: 'select_chapter',
		payload: chapterId
	};
};

export const saveChapterDetails = (chapterDetails) => {
	return {
		type: 'saved_chapter',
		payload: chapterDetails
	};
};
