import { combineReducers } from 'redux';
import BookReducer from './BookReducer';
import SelectionReducer from './SelectionReducer';
import ChapterDetailsReducer from './ChapterDetailsReducer.js';

export default combineReducers({
	chapters: BookReducer,
	selectedChapterId: SelectionReducer,
	selectedChapterDescription: ChapterDetailsReducer
});
