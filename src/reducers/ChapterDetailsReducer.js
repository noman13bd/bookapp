export default (state = null, action) => {
	switch (action.type) {
		case 'saved_chapter':
			return action.payload;				
		default:
			return state;
	}
};
