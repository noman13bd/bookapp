export default (state = null, action) => {
	switch (action.type) {
		case 'select_chapter':
			return action.payload;				
		default:
			return state;
	}
};
