import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Icon, Left, Body, Button } from 'native-base';
import { connect } from 'react-redux';

class ChapterDetails extends Component {
	render() {
		return (
			<Container>
				<Content>
					<Card style={{ flex: 0 }}>
						<CardItem>
							<Body>
								<Text>{this.props.description}</Text>
							</Body>
						</CardItem>
					</Card>
				</Content>
			</Container>
		);
	}
}

const mapStateToProps = state => {
  return { description: state.selectedChapterDescription };
};

export default connect(mapStateToProps)(ChapterDetails);
