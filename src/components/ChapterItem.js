import React, { Component } from 'react';
import { CardItem, Text } from 'native-base';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import * as actions from '../actions';

class ChapterItem extends Component {
	renderDescription() {
		const { chapter, expanded } = this.props;
		this.props.selectChapter(this.props.chapter.id);
		if (expanded) {
			this.props.saveChapterDetails(chapter.description);
			Actions.cd({ title: this.props.chapter.title });
		}		
	}

	render() {
		return (
			<View>
				<CardItem>
					<Text onPress={() => this.renderDescription()}>{this.props.chapter.title}</Text>					
				</CardItem>
			</View>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	const expanded = state.selectedChapterId === ownProps.chapter.id;
	return { expanded };
};

export default connect(mapStateToProps, actions)(ChapterItem);
