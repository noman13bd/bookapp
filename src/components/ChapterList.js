import React, { Component } from 'react';
import { ListView, BackHandler  } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Card, CardItem, Text } from 'native-base';
import ChapterItem from './ChapterItem';

class ChapterList extends Component {
  componentWillMount() {
    BackHandler.removeEventListener('backPress', this.handleBackButton);
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });

    this.dataSource = ds.cloneWithRows(this.props.chapters);
  } 

  componentDidMount() {
    BackHandler.addEventListener('backPress', this.handleBackButton);
  }

  handleBackButton() {
    //return true;
  }

  renderRow(chapter) {
    return <ChapterItem chapter={chapter} />;
  }

  render() {
    return (
      <Container>
        <Content>
          <Card>
            <CardItem header>
              <Text>Chapters</Text>
            </CardItem>
            <ListView 
              dataSource={this.dataSource}
              renderRow={this.renderRow}
            />
          </Card>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return { chapters: state.chapters };
};

export default connect(mapStateToProps)(ChapterList);
