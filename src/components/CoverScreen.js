import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Spinner } from 'native-base';

class App extends Component {
  componentDidMount() {
    setTimeout(
      () => { console.log('Chapter List Loading'); Actions.cl(); },
      500
    );
  }

  render() {
    return (
      <Image 
        source={require('../../images/paradoxical-sajid.jpg')}
        style={styles.backgroundImage}
      >
        <View style={styles.bottomView}>
          <Spinner color='green' />
          <Text style={styles.bottomText}>Loading</Text>
        </View>
      </Image>

    );
  }
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'stretch'
    },

    text: {
        textAlign: 'center',
        color: 'white',
        backgroundColor: 'rgba(0,0,0,0)',
        fontSize: 32
    },

    bottomView: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'flex-end',
      alignItems: 'center'
    },

    bottomText: {
      color: 'green',
      fontWeight: 'bold',
      fontSize: 15,
    }
});

export default App;
