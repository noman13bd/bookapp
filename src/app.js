import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from './reducers';
import CoverScreen from './components/CoverScreen';
import ChapterList from './components/ChapterList';
import ChapterDetails from './components/ChapterDetails';

class App extends Component {
  render() {
    return (
      <Provider store={createStore(reducers)}>
        <Router>
          <Scene key="root">
            <Scene 
              key="cs"
              component={CoverScreen}
              title=""
              hideNavBar="true"
              modal="true"
              initial
            />
            <Scene 
              key="cl"
              component={ChapterList}
              title=""            
              hideNavBar="true"
              modal="true"            
            />
            <Scene 
              key="cd"
              component={ChapterDetails}
              title="Chapter" 
              modal="true"            
            />
          </Scene>
        </Router>
      </Provider>
    );
  }
}

export default App;
